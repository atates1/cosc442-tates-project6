// Generated by Selenium IDE
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
import java.net.MalformedURLException;
import java.net.URL;
public class GoodBadAdminLogInTest {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @Before
  public void setUp() {
    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  public String waitForWindow(int timeout) {
    try {
      Thread.sleep(timeout);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    Set<String> whNow = driver.getWindowHandles();
    Set<String> whThen = (Set<String>) vars.get("window_handles");
    if (whNow.size() > whThen.size()) {
      whNow.removeAll(whThen);
    }
    return whNow.iterator().next();
  }
  @Test
  public void goodBadAdminLogIn() {
    driver.get("https://phptravels.com/demo/");
    driver.manage().window().setSize(new Dimension(1227, 777));
    {
      WebElement element = driver.findElement(By.linkText("Demo"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    js.executeScript("window.scrollTo(0,254)");
    {
      WebElement element = driver.findElement(By.cssSelector(".wow:nth-child(2) .btn"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    {
      WebElement element = driver.findElement(By.tagName("body"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element, 0, 0).perform();
    }
    vars.put("window_handles", driver.getWindowHandles());
    driver.findElement(By.cssSelector(".wow:nth-child(3) small:nth-child(1)")).click();
    vars.put("win743", waitForWindow(2000));
    vars.put("root", driver.getWindowHandle());
    driver.switchTo().window(vars.get("win743").toString());
    driver.findElement(By.name("email")).sendKeys("admin@phptravels.com");
    driver.switchTo().window(vars.get("root").toString());
    driver.switchTo().window(vars.get("win743").toString());
    driver.findElement(By.name("password")).sendKeys("demoadmin");
    driver.findElement(By.name("password")).sendKeys(Keys.ENTER);
    driver.findElement(By.cssSelector(".col-lg-8 > .sc-fYiAbW")).click();
    assertThat(driver.findElement(By.cssSelector("#mainHeader > strong")).getText(), is("Hi Admin!"));
    driver.findElement(By.cssSelector("#logout strong")).click();
    driver.findElement(By.name("email")).sendKeys("badloginadmin");
    driver.findElement(By.name("password")).sendKeys("thiswrongtoo");
    driver.findElement(By.cssSelector(".btn-block")).click();
    driver.findElement(By.name("email")).click();
    driver.findElement(By.name("email")).sendKeys("badloginadmin@phptravels.com");
    driver.findElement(By.cssSelector(".btn-block")).click();
    {
      WebElement element = driver.findElement(By.cssSelector(".btn-block"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    driver.findElement(By.cssSelector(".alert")).click();
    driver.findElement(By.cssSelector(".alert")).click();
    {
      WebElement element = driver.findElement(By.cssSelector(".alert"));
      Actions builder = new Actions(driver);
      builder.doubleClick(element).perform();
    }
    assertThat(driver.findElement(By.cssSelector(".alert")).getText(), is("Invalid Login Credentials"));
  }
}
